package ru.t1.stroilov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.stroilov.tm.enumerated.Role;
import ru.t1.stroilov.tm.util.TerminalUtil;

public class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    public final static String DESCRIPTION = "Login User.";

    @NotNull
    public final static String NAME = "login";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("Enter password:");
        @NotNull final String password = TerminalUtil.nextLine();
        getAuthService().login(login, password);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return null;
    }
}
