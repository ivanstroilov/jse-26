package ru.t1.stroilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.stroilov.tm.util.TerminalUtil;

public class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @NotNull
    public final static String DESCRIPTION = "Update Task by ID.";

    @NotNull
    public final static String NAME = "task-update-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("Enter id:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("Enter name:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        @NotNull final String description = TerminalUtil.nextLine();
        getTaskService().updateById(getUserId(), id, name, description);
    }
}
