package ru.t1.stroilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.api.repository.IProjectRepository;
import ru.t1.stroilov.tm.enumerated.Sort;
import ru.t1.stroilov.tm.enumerated.Status;
import ru.t1.stroilov.tm.model.Project;

import java.util.List;

public interface IProjectService extends IProjectRepository {

    @NotNull
    Project updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    Project updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Project changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    List<Project> findAll(@Nullable String userId, @Nullable Sort sort);

}
