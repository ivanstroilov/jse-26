package ru.t1.stroilov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.stroilov.tm.command.data.AbstractDataCommand;
import ru.t1.stroilov.tm.command.data.DataLoadBackupCommand;
import ru.t1.stroilov.tm.command.data.DataSaveBackupCommand;

import java.nio.file.Files;
import java.nio.file.Paths;

public class Backup extends Thread {

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    public void init() {
        load();
        start();
    }

    public void save() {
        bootstrap.processCommand(DataSaveBackupCommand.NAME, false);
    }

    public void load() {
        if (!Files.exists(Paths.get(AbstractDataCommand.FILE_BACKUP))) return;
        bootstrap.processCommand(DataLoadBackupCommand.NAME, false);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            Thread.sleep(3000);
            save();
        }
    }

}

